Résumé Reunion

Le sujet de bureau d'études consiste au développement de la base d'un framework sur les SMA (systèmes multi-agents) codé en python. Le projet se décompose en deux grandes parties : une partie noyau et une partie IHM. 
L'équipe se divise en trois groupes distincts :
    - un groupe de développement du noyau (2 personnes)
    - un groupe de développement de l'IHM (1 personne)
    - un groupe de testeurs/architectes logiciel (2 personnes, 1 par groupe de développement)

Dans un premier temps le groupe travaille d'un seul bloc afin de réfléchir à l'architecture logicielle du projet avant de la mettre en oeuvre.  

La répartition du groupe se fait de cette manière :
    - Ronan Kennedy développeur du noyau
    - Wissam Benadjaoud développeuse du noyau
    - Aurélie Akli développeuse de l'IHM
    - Sébastien Goyon architecte/testeur du noyau
    - Jérémie Drezen architecte/testeur de l'IHM
