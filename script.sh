#! /bin/sh
#console command
PYTHON="python" #console command for python
PIP="pip3" #console command for pip

#bool
NEWINSTALL=true # true : New project installation / false : project update
LAUNCHMAIN=true # true : launch main before exit
LocalOnly=false # true : don't watch online updates, just compile the local version

#branch
COREBRANCH="master"
IHMBRANCH="master"
PHILBRANCH="master"

function compilelib {
	$PYTHON setup.py bdist_wheel
	$PIP install --force-reinstall $1
}

function updateremote {
	BranchToCompil=$1
	IsLib=$2
	PathToLib=$3

	CURRENTBRANCH=$(eval "git rev-parse --abbrev-ref HEAD")
	git stash
	git checkout master
	git pull
	git checkout $BranchToCompil
	git pull

	if [ $IsLib = true ]; then
		compilelib "$PathToLib"
	else
		if [ $LAUNCHMAIN = true ]; then
			$PYTHON ./philosophersLaunchExample.py
		else
			read -p "Main is ready, press any key to unstash work and return to older version"
		fi
	fi
	git checkout $CURRENTBRANCH
	git stash pop
}

if [ $NEWINSTALL = true ]; then
    $PIP install matplotlib
    $PIP install wheel
    git clone https://gitlab.com/be-pyamak/pyamak-ihm.git
    git clone https://gitlab.com/be-pyamak/pyamak-noyau.git
    git clone https://gitlab.com/be-pyamak/philosopher_example.git
fi

if [ $LocalOnly = false ]; then
	cd ./pyamak-noyau/
	updateremote "$COREBRANCH" "true" "dist/pyAmakCore-0.0.1-py3-none-any.whl"

	cd ../pyamak-ihm/
	updateremote "$COREBRANCH" "true" "dist/pyAmakIHM-0.0.1-py3-none-any.whl"

	cd ../philosopher_example/
	updateremote "$COREBRANCH" "false" ""
	cd ../
else
	cd ./pyamak-noyau/
	compilelib "dist/pyAmakCore-0.0.1-py3-none-any.whl"

	cd ../pyamak-ihm/
	compilelib "dist/pyAmakIHM-0.0.1-py3-none-any.whl"

	cd ../philosopher_example/
	if [ $LAUNCHMAIN = true ]; then
		$PYTHON ./philosophersLaunchExample.py
	fi
fi
